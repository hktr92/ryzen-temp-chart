This repository is intended to fetch CPU and GPU temperatures from `data/` directory, crunch them, and output a chart-friendly JSON.

>**NOTE**: the API is unstable, so it won't give any output (files are too large). multithreading will be implemented for better performance!

Directory structure:
- `backend/` -> a Deno-powered API, written in TypeScript
- `data/` -> where raw temperature data is put
- `ui/` -> (tbd) a simple Vue 3 app

Computer specs:
- CPU: AMD Ryzen 5 3600X
- GPU: AMD Radeon 5600
- Mem: 2x8GB DDR4, dual-channel
- SSD: some Intel M.2 drive

System info:
- Manjaro Linux 20.1 testing channel
- Linux Kernel 5.8.0 release candidate 6
- KDE Plasma 5.19.3

About `data` directory:
- `ac_syndicate` and `ac_unity` are temps benchmarking **before** any FAN tunning and **before** putting an extra Noctua fan for the airflow output
- `ac_noctua_notuning` are temps benchmarking **before** any FAN tunning
- `ac_noctua_tuning` are temps benchmarking **after** investing ~2h of manual FAN tunning for better performance (high spin, low temps)
