import { LsRes } from "https://deno.land/x/easypath/mod.ts"
import { getLines } from "../util/getLines.ts"
import { getFiles } from "../util/getFiles.ts"
import { Metric } from "../entity/Metric.ts"

const route = {
    path: "/metrics/:project",
    method: "get",
    handler({ response, params }: any) {
        console.debug("/metrics/:project/:file", params)

        const { project } = params
        const metrics: Record<string, Metric[]> = {}

        const rawMetrics = getFiles(project)

        rawMetrics.forEach((file: LsRes) => {
            const filePath = `../data/${project}/${file.name}`

            const iterator = getLines(filePath)
            while (!iterator.done) {
                metrics[file.name].push({})
            }
            
        })

        console.debug("metrics", metrics)

        response.type = 'application/json'
        response.body = metrics

        console.debug("response", response)
    }
}

export { route }