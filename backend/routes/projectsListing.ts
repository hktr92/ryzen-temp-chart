import { getProjects } from "../util/getProjects.ts"

const route = {
    path: "/projects",
    method: "get",
    handler({ response }: any) {
        response.type = 'application/json'
        response.body = Array.from(getProjects())
    }
}

export { route }