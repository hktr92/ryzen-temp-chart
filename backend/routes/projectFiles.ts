import { getFiles } from "../util/getFiles.ts"

const route = {
    path: "/project/:project",
    method: "get",
    handler({ params, response }: any) {
        const { project } = params

        response.type = 'application/json'

        if (!project) {
            response.body = []
        } else {
            response.body = getFiles(project)
        }
    }
}

export { route }