import { path, LsRes } from "https://deno.land/x/easypath/mod.ts"
import { basePath } from "./basePath.ts"

// UTIL
const getFiles = (project: string): LsRes[] => {
    return path(`${basePath}/${project}`).ls().filter((res: LsRes) => res.isFile)
}

export { getFiles }