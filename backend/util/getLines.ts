import { readLines } from "https://deno.land/std/io/mod.ts";

function* getLines(filePath: string) {
    let fileReader = await Deno.open(filePath)

    for await (let line of readLines(fileReader)) {
        yield line
    }
}

export { getLines }