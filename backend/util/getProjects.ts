import { path, LsRes } from "https://deno.land/x/easypath/mod.ts"
import { basePath } from "./basePath.ts"

const getProjects = (): LsRes[] => {
    return path(basePath).ls().filter((res: LsRes) => res.isDirectory)
}

export { getProjects }