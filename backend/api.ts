import { Application, Router } from "https://deno.land/x/oak/mod.ts"
import { path } from "https://deno.land/x/easypath/mod.ts"

// ROUTER
const router = new Router()

path('./routes')
    .ls()
    .filter((ent: any) => ent.isFile)
    .forEach(async (ent: any) => {
        const { route } = await import(`./routes/${ent.name}`)
        const { method, path, handler } = route

        //@ts-ignore
        router[method](path, handler)
    })

// APP
const app = new Application()
app.use(router.routes())
app.use(router.allowedMethods())

// BOOTSTRAP
await app.listen({ port: 8000 })