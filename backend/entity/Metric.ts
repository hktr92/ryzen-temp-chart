export interface Metric {
    date: number
    host: string
    sensor: string
    temperature: number
}